pragma solidity ^0.8.3;

contract ERC20Basic {
    string public constant name = "ERC20Basic";
    string public constant symbol = "ERC";
    uint8 public constant decimals = 18;
    mapping(address => uint256) balances;
    using SafeMath for uint256;

    event Transfer(address indexed from, address indexed to, uint tokens);

    //written by me and it's not from ERC20 standard
    function setBalance(address _address, uint256 _balance) internal {
        balances[_address] = _balance;
    }

    function balanceOf(address tokenOwner) public view returns (uint256) {
        return balances[tokenOwner];
    }

    function transfer(address receiver, uint256 numTokens) public returns (bool) {
        require(numTokens <= balances[msg.sender]);
        balances[msg.sender] = balances[msg.sender].sub(numTokens);
        balances[receiver] = balances[receiver].add(numTokens);
        emit Transfer(msg.sender, receiver, numTokens);
        return true;
    }

    function transferFrom(address owner, address buyer, uint256 numTokens) public returns (bool) {
        require(numTokens <= balances[owner]);
        balances[owner] = balances[owner].sub(numTokens);
        balances[buyer] = balances[buyer].add(numTokens);
        emit Transfer(owner, buyer, numTokens);
        return true;
    }
}

library SafeMath {
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        assert(b <= a);
        return a - b;
    }

    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        assert(c >= a);
        return c;
    }
}

contract TransactionHandler {
    //    ERC20Basic public token = new ERC20Basic();
    mapping(address => bytes32) members; // using bytes32 instead of string
    mapping(bytes32 => bool) isTaken;

    event Completed(address transferFrom, address transferTo, uint256 amount);

    function isMember(address member) public view returns (bool isIndeed) {
        return (members[member] != 0);
    }

    function isSet(bytes32 name) public view returns (bool isIndeed) {
        return (isTaken[name]);
    }

    function login(bytes32 _name) public returns (bool success){
        require(!isMember(msg.sender), "Account exists!");
        require(!isSet(_name), "The username has been already used!");
        members[msg.sender] = _name;
        isTaken[_name] = true;
        return true;
    }

    function changeUsername(bytes32 _name) public returns (bool success){
        require(isMember(msg.sender), "You must login first!");
        require(!isSet(_name), "The username has been already used!");
        isTaken[members[msg.sender]] = false;
        members[msg.sender] = _name;
        isTaken[_name] = true;
        return true;
    }

    function transaction(address _target) payable public returns (bool success){
        uint256 _amount = msg.value;
        require(_amount > 0, "You need to send some ether");
        //        token.transferFrom(msg.sender, _target, _amount);
        (bool sent, bytes memory data) = _target.call{value : _amount}("");
        require(sent, "Failed to send Ether");
        emit Completed(msg.sender, _target, _amount);
        return true;
    }
}