import 'package:ether_wallet/utility/style_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

Color get mainColor =>
    Get.context == null ? Styles.mainBlue : Get.theme.primaryColor;
