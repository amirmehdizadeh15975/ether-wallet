import 'package:ether_wallet/screens/home_screen/home_model.dart';
import 'package:ether_wallet/screens/login_screen/login_model.dart';
import 'package:ether_wallet/screens/transaction_screen/transaction_model.dart';
import 'package:ether_wallet/screens/welcome_screen/welcome_model.dart';
import 'package:flutter/foundation.dart';
import 'package:web3dart/web3dart.dart';

class MainModel
    with
        ChangeNotifier,
        DiagnosticableTreeMixin,
        TransactionModel,
        HomeModel,
        LoginModel,
        WelcomeModel {
  bool _loading = false;

  bool get loading => _loading;

  void setLoading(bool val) {
    _loading = val;
    notifyListeners();
  }

  Web3Client? _client;

  Web3Client? get client => _client;

  void setClient(Web3Client? client) {
    _client = client;
    notifyListeners();
  }

  Credentials? _credentials;

  Credentials? get credentials => _credentials;

  void setCredentials(Credentials? credentials) {
    _credentials = credentials;
    notifyListeners();
  }
}
