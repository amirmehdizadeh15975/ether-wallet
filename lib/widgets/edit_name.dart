import 'package:ether_wallet/utility/get_utils.dart';
import 'package:ether_wallet/utility/style_manager.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

class EditNameDialog extends StatefulWidget {
  final String? name;

  const EditNameDialog({Key? key, this.name}) : super(key: key);

  @override
  State<EditNameDialog> createState() => _EditNameDialogState();
}

class _EditNameDialogState extends State<EditNameDialog> {
  TextEditingController valueC = TextEditingController();

  @override
  void initState() {
    valueC.text = widget.name ?? '';
    // valueC.selection =
    //     TextSelection(baseOffset: 0, extentOffset: valueC.text.length);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      elevation: 0.0,
      backgroundColor: Colors.white.withOpacity(0.9),
      child: SingleChildScrollView(child: dialogContent()),
    );
  }

  Widget dialogContent() {
    return Container(
      constraints: BoxConstraints(maxHeight: Get.height * 0.3),
      padding: const EdgeInsets.all(12),
      child: Column(
        children: [
          Row(children: [
            Text("Set a New Username",
                style: TextStyle(
                    color: Styles.mainBlue,
                    fontWeight: FontWeight.bold,
                    fontSize: 18)),
            const Spacer(),
            IconButton(
                onPressed: () => Get.back(),
                icon: const Icon(Icons.close_rounded, color: Colors.blueGrey))
          ]),
          const Spacer(),
          TextField(
            autofocus: true,
            showCursor: true,
            textAlign: TextAlign.center,
            style: TextStyle(
                color: Colors.blueGrey.shade600,
                fontSize: 22,
                fontWeight: FontWeight.bold),
            decoration: const InputDecoration(
              filled: true,
              border: InputBorder.none,
            ),
            controller: valueC,
          ),
          const Spacer(),
          ElevatedButton(
            onPressed: () => valueC.text.length < 4
                ? showGetToast(
                    'Warning', "Your username's length must be at least 4!")
                : Get.back(result: valueC.text),
            child: const Text('Confirm',
                style: TextStyle(fontWeight: FontWeight.bold)),
          ),
        ],
      ),
    );
  }
}
