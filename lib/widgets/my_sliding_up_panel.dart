import 'package:ether_wallet/globals/main_model.dart';
import 'package:ether_wallet/utility/style_manager.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

class MySlidingUpPanel extends StatelessWidget {
  final PanelController pc = PanelController();
  final bool isUpdate;
  final String title;
  final String body;
  final Function onConfirm;
  final Function onReject;

  MySlidingUpPanel(
      {Key? key,
      this.isUpdate = false,
      required this.title,
      required this.body,
      required this.onConfirm,
      required this.onReject})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    MainModel model = context.watch<MainModel>();
    double width = MediaQuery.of(context).size.width;
    double space = width * 0.025;
    return Container(
      margin: const EdgeInsets.all(8),
      child: SlidingUpPanel(
        controller: pc,
        defaultPanelState: PanelState.OPEN,
        borderRadius: BorderRadius.circular(10),
        // minHeight: Get.height * 0.023,
        // maxHeight: Get.height * 0.227,
        minHeight: space * 2,
        maxHeight: space * 19.0,
        panel: Column(children: [
          Container(
            height: 6,
            width: 45,
            margin: EdgeInsets.only(top: space - 3),
            decoration: BoxDecoration(
                color: Colors.grey.shade300,
                borderRadius: BorderRadius.circular(3)),
          ),
          Container(
            width: width,
            margin: EdgeInsets.only(left: 35, top: space - 3),
            child: Text(title,
                style: TextStyle(
                    color: isUpdate ? Styles.mainGreen : Styles.mainRed,
                    fontWeight: FontWeight.bold,
                    fontSize: 22)),
          ),
          Container(
            width: width,
            margin: EdgeInsets.only(left: 35, top: space * 2, right: 35),
            child: Text(body, style: const TextStyle(fontSize: 15)),
          ),
          const Spacer(),
          Container(
            margin: EdgeInsets.only(left: 27, bottom: space),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextButton(
                    onPressed: () {
                      if (!isUpdate) Get.back();
                      onConfirm();
                    },
                    child: Text(isUpdate ? 'Update Now' : 'Retry'),
                    style: TextButton.styleFrom(
                        primary: isUpdate ? Styles.mainGreen : Styles.mainRed,
                        textStyle: const TextStyle(fontSize: 16))),
                model.forceRetry
                    ? Container()
                    : TextButton(
                        onPressed: () => onReject(),
                        child: Text(isUpdate ? 'Later' : 'Close'),
                        style: TextButton.styleFrom(
                            primary: Colors.grey.shade300,
                            textStyle: const TextStyle(fontSize: 16)),
                      ),
              ],
            ),
          ),
        ]),
      ),
    );
  }
}
