import 'package:ether_wallet/utility/style_manager.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TransactionResult extends StatelessWidget {
  final List<String> results;

  const TransactionResult({Key? key, this.results = const []})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints(maxHeight: Get.height * 0.5),
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(10)),
      padding: const EdgeInsets.all(20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(children: [
            Text("Transaction Succeed",
                style: TextStyle(
                    color: Styles.mainGreen,
                    fontWeight: FontWeight.bold,
                    fontSize: 20)),
            const Spacer(),
            IconButton(
                onPressed: () => Get.back(),
                icon: Icon(Icons.close_rounded,
                    color: Styles.mainBlack.withOpacity(0.5)))
          ]),
          const SizedBox(height: 10),
          Divider(color: Styles.mainDivider, height: 1, thickness: 1),
          const Spacer(),
          const Text("From:", style: TextStyle(fontWeight: FontWeight.bold)),
          Container(
            alignment: Alignment.centerRight,
            child: Text(results[0],
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    fontWeight: FontWeight.bold, color: Styles.mainLogo2)),
          ),
          const Spacer(),
          const Text("To:", style: TextStyle(fontWeight: FontWeight.bold)),
          Container(
            alignment: Alignment.centerRight,
            child: Text(results[1],
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    fontWeight: FontWeight.bold, color: Styles.mainLogo2)),
          ),
          const Spacer(),
          const Text("Amount:", style: TextStyle(fontWeight: FontWeight.bold)),
          Container(
            alignment: Alignment.centerRight,
            child: Text(results[2],
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    fontWeight: FontWeight.bold, color: Styles.mainLogo2)),
          ),
          const Spacer(),
          Divider(color: Styles.mainDivider, height: 1, thickness: 1),
          const SizedBox(height: 10),
          Align(
            alignment: Alignment.center,
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  primary: Styles.mainLogo1,
                  fixedSize: Size(Get.width * 0.5, 40)),
              onPressed: () => Get.back(),
              child: Text('Back to Home',
                  style: TextStyle(
                      fontWeight: FontWeight.bold, color: Styles.mainLogo2)),
            ),
          ),
        ],
      ),
    );
  }
}
