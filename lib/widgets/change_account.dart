import 'package:ether_wallet/globals/main_model.dart';
import 'package:ether_wallet/utility/strings_manager.dart';
import 'package:ether_wallet/utility/style_manager.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:provider/provider.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:web3dart/web3dart.dart';

class ChangeAccountDialog extends StatefulWidget {
  const ChangeAccountDialog({this.selectedIndex = 0, Key? key})
      : super(key: key);
  final int selectedIndex;

  @override
  State<ChangeAccountDialog> createState() => _ChangeAccountDialogState();
}

class _ChangeAccountDialogState extends State<ChangeAccountDialog> {
  int selectedIndex = 0;
  bool loading = false;

  @override
  void initState() {
    selectedIndex = widget.selectedIndex;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    MainModel model = context.watch<MainModel>();
    return Container(
      constraints: BoxConstraints(maxHeight: Get.height * 0.6),
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(10)),
      padding: const EdgeInsets.all(20),
      child: Column(
        children: [
          Row(children: [
            Text("Switch Account",
                style: TextStyle(
                    color: Styles.mainBlack,
                    fontWeight: FontWeight.bold,
                    fontSize: 20)),
            const Spacer(),
            IconButton(
                onPressed: () => Get.back(),
                icon: Icon(Icons.close_rounded,
                    color: Styles.mainBlack.withOpacity(0.5)))
          ]),
          const SizedBox(height: 10),
          Divider(color: Styles.mainDivider, height: 1, thickness: 1),
          Expanded(
            child: ListView.builder(
              shrinkWrap: true,
              itemCount: model.accountNames.length,
              itemBuilder: (context, index) => Container(
                margin: const EdgeInsets.only(top: 8),
                decoration: BoxDecoration(
                    color: Styles.mainLogo0,
                    borderRadius: BorderRadius.circular(8)),
                child: RadioListTile(
                  value: index,
                  groupValue: selectedIndex,
                  activeColor: Styles.mainLogo2,
                  onChanged: (int? i) =>
                      setState(() => selectedIndex = i ?? -1),
                  title: Text(model.accountNames[index],
                      style: TextStyle(
                          fontSize: 17,
                          fontWeight: FontWeight.bold,
                          color: Styles.mainBlack)),
                  dense: true,
                ),
              ),
            ),
          ),
          Divider(color: Styles.mainDivider, height: 1, thickness: 1),
          const SizedBox(height: 10),
          ElevatedButton(
            style: ElevatedButton.styleFrom(
                primary: Styles.mainLogo1, fixedSize: const Size(90, 40)),
            onPressed: loading ? null : () => confirmAndNavigate(model),
            child: loading
                ? SpinKitThreeBounce(color: Styles.mainLogo2, size: 20)
                : Text('Confirm',
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: Styles.mainLogo2)),
          ),
        ],
      ),
    );
  }

  confirmAndNavigate(MainModel model) async {
    setState(() => loading = true);
    String selectedKey = model.accountKeys[selectedIndex];
    Credentials? _client =
        await model.client?.credentialsFromPrivateKey(selectedKey);
    model.setCredentials(_client);
    SharedPreferences sp = await SharedPreferences.getInstance();
    model.setSelectedKey(selectedKey);
    sp.setString(Strings.selectedKey, selectedKey);
    Get.offAllNamed('/home');
  }
}
