import 'package:ether_wallet/globals/main_model.dart';
import 'package:ether_wallet/utility/strings_manager.dart';
import 'package:ether_wallet/utility/style_manager.dart';
import 'package:ether_wallet/widgets/change_account.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LeftDrawer extends StatelessWidget {
  const LeftDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = Get.width;
    MainModel model = context.watch<MainModel>();
    return Container(
      constraints: BoxConstraints(maxWidth: width * 0.75),
      decoration: const BoxDecoration(
          borderRadius: BorderRadius.only(
              bottomRight: Radius.circular(12), topRight: Radius.circular(12)),
          color: Colors.white),
      child: Column(children: [
        const SizedBox(height: 10),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const SizedBox(width: 10),
            ClipRect(
              child: Image.asset('assets/images/etherLogo.png',
                  fit: BoxFit.fitHeight, height: 30, width: 30),
            ),
            const SizedBox(width: 4),
            const Padding(
              padding: EdgeInsets.only(top: 5.0),
              child: Text(
                "Wallet Management",
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
            ),
            const Spacer(),
            IconButton(
                icon: const Icon(Icons.arrow_back_ios),
                onPressed: () => Navigator.pop(context)),
            const SizedBox(width: 5),
          ],
        ),
        const Divider(thickness: 1),
        Expanded(
          child: ListView(children: [
            ListTile(
                leading: const Icon(Icons.account_circle),
                title: const Text("Add an account"),
                onTap: () {
                  Get.back();
                  Get.toNamed('/login');
                }),
            ListTile(
              leading: const Icon(Icons.switch_account),
              title: const Text("Change account"),
              onTap: () {
                Get.back();
                Get.bottomSheet(ChangeAccountDialog(
                    selectedIndex: model.accountKeys
                        .indexWhere((k) => k == model.selectedKey)));
              },
            ),
            // ListTile(
            //     leading: Icon(FontAwesome.qrcode),
            //     title: Text("Scan New QR"),
            //     onTap: () {}),
            // ListTile(
            //     leading: Icon(FontAwesome.info_circle),
            //     title: Text("About Us"),
            //     onTap: () {}),
            // ListTile(
            //     leading: Icon(FontAwesome.phone),
            //     title: Text("Contact Us"),
            //     onTap: () {}),
          ]),
        ),
        const Divider(),
        Center(
            child: TextButton(
                onPressed: () => deleteAccount(model),
                child: Text("Remove this Account",
                    style: TextStyle(color: Styles.mainBlue)))),
      ]),
    );
  }

  deleteAccount(MainModel model) async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    List<String> names = model.accountNames;
    List<String> keys = model.accountKeys;
    int removeIndex = keys.indexWhere((k) => k == model.selectedKey);
    names.removeAt(removeIndex);
    keys.removeAt(removeIndex);
    sp.setStringList(Strings.accountNames, names);
    sp.setStringList(Strings.accountKeys, keys);
    if (keys.isEmpty) {
      model.setSelectedKey('');
      sp.remove(Strings.selectedKey);
      Get.back();
      Get.offAllNamed('/login');
    } else {
      model.setSelectedKey(keys.last);
      sp.setString(Strings.selectedKey, model.selectedKey);
      Get.back();
      Get.offAllNamed('/home');
    }
  }
}
