import 'package:device_info/device_info.dart';
import 'package:ether_wallet/utility/api_manager.dart';
import 'dart:io';
import 'package:network_manager/network_manager.dart';
import 'package:uuid/uuid.dart';

class Api {
  static Future<NetworkResponse> postItem({required Function retry}) async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    String sessionID = const Uuid().v4();
    String deviceTypeID = Platform.isAndroid
        ? "5D350590-50A7-4245-B344-AE3C37C991E4"
        : "C8D61E16-0619-4BBC-A8C5-A929ED8C8F6B";
    String deviceID = '';
    if (Platform.isAndroid) {
      AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      deviceID = androidInfo.id;
    } else {
      IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      deviceID = iosInfo.identifierForVendor;
    }
    final data = {
      "DeviceTypeKey": deviceTypeID,
      "DeviceKey": deviceID,
      "SessionKey": sessionID
    };
    String api = Apis.baseUrl + '';
    return NetworkRequest(api: api, data: data, retry: retry).post();
  }

  static Future<NetworkResponse> getItem({required Function retry}) async {
    const data = '';
    String api = Apis.baseUrl + '';
    return NetworkRequest(api: api, data: data, retry: retry).get();
  }
}
