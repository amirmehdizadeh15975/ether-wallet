import 'package:intl/intl.dart';

class Strings {
  Strings._();

  static String accountNames = "AccountNames";
  static String accountKeys = "AccountKeys";
  static String selectedKey = "SelectedKey";
}

/// A StandardString Helper
class StandardString {
  String standardTime(DateTime dateTime) {
    String result = (dateTime.hour < 10 ? "0" : "") +
        dateTime.hour.toString() +
        ":" +
        (dateTime.minute < 10 ? "0" : "") +
        dateTime.minute.toString();
    return result;
  }

  String standardDuration(Duration duration) {
    int dHour = (duration.inMinutes / 60).floor();
    int dMin = (duration.inMinutes % 60);
    int dSec = (duration.inSeconds % 3600);
    String result = (dHour < 10 ? "0" : "") +
        dHour.toString() +
        ":" +
        (dMin < 10 ? "0" : "") +
        dMin.toString() +
        ":" +
        (dSec < 10 ? "0" : "") +
        dSec.toString();
    return result;
  }

  String standardDate(DateTime dateTime, {String format = "yyyy-MM-dd"}) {
    String result;
    DateFormat f = DateFormat(format);
    result = f.format(dateTime);
    return result;
  }

  int getJulianDate() {
    int diffInDays =
        DateTime.now().difference(DateTime(DateTime.now().year, 1, 1)).inDays +
            1;
    return diffInDays;
  }

  DateTime getDateFromJulianDate(int julian) {
    return DateTime(DateTime.now().year, 1, 1).add(Duration(days: julian));
  }

  Duration durationFromString(String dStr) {
    if (dStr.length > 8) {
      dStr = dStr.substring(dStr.length - 8, dStr.length);
      int hours = int.parse(dStr.split(":")[0]);
      int mins = int.parse(dStr.split(":")[1]);

      return Duration(hours: hours, minutes: mins);
    } else {
      return const Duration(seconds: 0);
    }
  }
}
