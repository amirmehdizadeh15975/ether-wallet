import 'package:flutter/material.dart';
import 'package:get/get.dart';

showGetToast(String title, String msg,
    {Function? onDone, int? duration, Color? color, SnackPosition? position}) {
  Get.snackbar(
    title,
    msg,
    colorText: Colors.white,
    // message
    icon: const Icon(Icons.error, color: Colors.white),
    shouldIconPulse: true,
    backgroundColor: (color ?? Colors.red).withOpacity(0.6),
    onTap: (v) {},
    snackPosition: position ?? SnackPosition.TOP,
    barBlur: 5,
    isDismissible: true,
    snackbarStatus: (status) {
      if (status == SnackbarStatus.CLOSED && onDone != null) {
        onDone();
      }
    },
    duration: Duration(seconds: duration ?? 2),
  );
}
