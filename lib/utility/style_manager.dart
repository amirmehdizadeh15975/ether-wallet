import 'package:flutter/material.dart';

class Styles {
  Styles._();

  static Color niceGreen = const Color(0xff32bd26);
  static Color mainGreen = const Color(0xff18bc9c);
  static Color mainBlack = const Color(0xff333333);
  static Color mainBlue = const Color(0xff4e92ff);
  static Color mainRed = const Color(0xffff4e4e);
  static Color mainOrange = const Color(0xffffa04e);
  static Color mainDivider = const Color(0xfff1f1f1);
  static Color mainLogo0 = const Color(0xffecf0f1); //LightGrey
  static Color mainLogo1 = const Color(0xff3c3c3d); //Grey
  static Color mainLogo2 = const Color(0xffc99d66); //Gold
}
