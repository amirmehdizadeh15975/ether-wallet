import 'dart:convert';
import 'package:ether_wallet/globals/main_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:http/http.dart';
import 'package:provider/provider.dart';
import 'package:web3dart/web3dart.dart';
import 'package:web_socket_channel/io.dart';

class ContractLinking extends ChangeNotifier {
  final MainModel model = Get.context!.watch<MainModel>();
  final String _rpcUrl = "HTTP://192.168.1.103:7545";
  final String _wsUrl = "ws://192.168.1.103:7545/";
  final String _privateKey =
      "0ccb3c4e32f920fe748f0592602f94d0311d91b9e1903bf524924d7d3ef961d1";
  late Web3Client _client;
  late String _abiCode;
  late EthereumAddress _contractAddress;
  late Credentials _credentials;
  late DeployedContract _contract;
  late ContractFunction _yourName;
  late ContractFunction _setName;
  String deployedName = '';

  initialSetup() async {
    // establish a connection to the ethereum rpc node. The socketConnector
    // property allows more efficient event streams over websocket instead of
    // http-polls. However, the socketConnector property is experimental.
    _client = Web3Client(_rpcUrl, Client(),
        socketConnector: () =>
            IOWebSocketChannel.connect(_wsUrl).cast<String>());
    // _client = Web3Client(_rpcUrl, Client());

    await getAbi();
    await getCredentials();
    await getDeployedContract();
  }

  Future<void> getAbi() async {
    // Reading the contract abi
    String abiStringFile =
        await rootBundle.loadString("src/artifacts/TransactionHandler.json");
    var jsonAbi = jsonDecode(abiStringFile);
    _abiCode = jsonEncode(jsonAbi["abi"]);

    _contractAddress =
        EthereumAddress.fromHex(jsonAbi["networks"]["5777"]["address"]);
  }

  Future<void> getCredentials() async {
    _credentials = await _client.credentialsFromPrivateKey(_privateKey);
  }

  Future<void> getDeployedContract() async {
    // Telling Web3dart where our contract is declared.
    _contract = DeployedContract(
        ContractAbi.fromJson(_abiCode, "TransactionHandler"), _contractAddress);

    // Extracting the functions, declared in contract.
    _yourName = _contract.function("yourName");
    _setName = _contract.function("setName");
    getName();
  }

  getName() async {
    // Getting the current name declared in the smart contract.
    _client.call(contract: _contract, function: _yourName, params: []).then(
        (value) {
      var currentName = value;
      deployedName = currentName[0];
      model.setLoading(false);
    });
  }

  setName(String nameToSet) async {
    // Setting the name to nameToSet(name defined by user)
    model.setLoading(true);
    await _client.sendTransaction(
        _credentials,
        Transaction.callContract(
            contract: _contract, function: _setName, parameters: [nameToSet]));
    getName();
  }
}
// my testing contract
/*
// SPDX-License-Identifier: MIT
pragma solidity ^0.5.9;

contract TransactionHandler {
  string public yourName;
  constructor() public {
  yourName = "Amir";
  }
  function setName(string memory nm) public{
           yourName = nm ;
  }
}
 */
