import 'package:ether_wallet/globals/main_model.dart';
import 'package:ether_wallet/utility/style_manager.dart';
import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'globals/app_config.dart';
import 'screens/home_screen/home_view.dart';
import 'screens/login_screen/login_view.dart';
import 'screens/splash_screen/splash_view.dart';
import 'screens/transaction_screen/transaction_view.dart';
import 'screens/welcome_screen/welcome_view.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    MainModel _model = context.watch<MainModel>();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return GetMaterialApp(
      title: 'Ether Wallet',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          scaffoldBackgroundColor: Colors.white,
          backgroundColor: Colors.white,
          primaryColor: Styles.mainLogo1,
          canvasColor: Colors.transparent,
          focusColor: Styles.mainLogo2,
          hoverColor: Styles.mainLogo2,
          shadowColor: Styles.mainLogo2,
          splashColor: Styles.mainLogo2,
          highlightColor: Styles.mainLogo2,
          dividerColor: Styles.mainDivider,
          appBarTheme: AppBarTheme(color: mainColor),
          fontFamily: 'AdobeClean'),
      home: SplashView(_model),
      routes: {
        '/splashScreen': (BuildContext context) => SplashView(_model),
      },
      getPages: [
        GetPage(name: "/splash", page: () => SplashView(_model)),
        GetPage(name: "/welcome", page: () => WelcomeView(_model)),
        GetPage(name: "/login", page: () => LoginView(_model)),
        GetPage(name: "/home", page: () => HomeView(_model)),
        GetPage(name: "/transaction", page: () => TransactionView(_model)),
      ],
      onGenerateRoute: (RouteSettings setting) {
        final List<String> pathElements = setting.name!.split("/");
        if (pathElements[0] == "") {
          return null;
        } else {
          return null;
        }
      },
      onUnknownRoute: (RouteSettings setting) {
        return MaterialPageRoute(
            builder: (BuildContext context) => SplashView(_model));
      },
    );
  }
}
