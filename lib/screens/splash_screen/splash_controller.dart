import 'dart:async';
import 'package:ether_wallet/globals/main_controller.dart';
import 'package:ether_wallet/globals/main_model.dart';
import 'package:ether_wallet/utility/api_manager.dart';
import 'package:ether_wallet/utility/strings_manager.dart';
import 'package:ether_wallet/widgets/my_sliding_up_panel.dart';
import 'package:get/get.dart';
import 'package:network_manager/network_manager.dart';
import 'package:package_info/package_info.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashController extends MainController {
  SplashController(MainModel model) : super(model);
  RxString appVersion = ''.obs;

  @override
  void onInit() {
    getFromSP();
    getVersion().then((s) => appVersion.value = s);
    initializeNetworkManager();
    // getTrackPermission();
    splashDelay();
    super.onInit();
  }

  initializeNetworkManager({String token = ''}) {
    /// initialize network manager
    NetworkOption.initialize(
        baseUrl: Apis.baseUrl,
        timeout: 30000,
        token: token,
        headers: {
          'Content-Type': 'application/json',
          "Authorization": "Bearer $token",
        },
        onStartDefault: () {
          model.setLoading(true);
        },
        onEndDefault: () {
          model.setLoading(false);
        },
        onSuccessDefault: (res) {},
        onFailedDefault: (NetworkResponse res) {
          if (res.extractedMessage!.toLowerCase().contains('connection')) {
            Get.bottomSheet(
                MySlidingUpPanel(
                    title: 'No Network',
                    body:
                        'You seem to be disconnected. Please check your internet connection.',
                    onConfirm: res.retryFunction ?? () {},
                    onReject: Get.back),
                isScrollControlled: true,
                isDismissible: false,
                enableDrag: false);
          } else {
            Get.bottomSheet(
                MySlidingUpPanel(
                    title: 'Error',
                    body: res.extractedMessage ?? '',
                    onConfirm: res.retryFunction ?? () {},
                    onReject: Get.back),
                isScrollControlled: true,
                isDismissible: false,
                enableDrag: false);
          }
        },
        errorMsgExtractor: (res) {
          return res["Message"] ?? "Unknown Error";
        },
        tokenExpireRule: (NetworkResponse response) {
          return response.responseCode == 401;
        },
        onTokenExpire: (NetworkResponse res) {
          handleTokenExpire(res);
        },
        successMsgExtractor: (res) {
          return res["Message"] ?? "Done";
        });
    NetworkOption.setAccessToken(token);
  }

  // getTrackPermission() async {
  //   // Privacy - Tracking Usage Description : must be added!
  //   final status = await AppTrackingTransparency.requestTrackingAuthorization();
  //   return status;
  // }

  Future<void> splashDelay() async {
    await Future.delayed(const Duration(seconds: 4));
    navigateToNextPage();
  }

  void handleTokenExpire(NetworkResponse res) async {}

  Future<String> getVersion() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    return packageInfo.version;
  }

  Future<void> getFromSP() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    List<String>? names = sp.getStringList(Strings.accountNames);
    List<String>? keys = sp.getStringList(Strings.accountKeys);
    String? key = sp.getString(Strings.selectedKey);
    model.setAccountNames(names ?? []);
    model.setAccountKeys(keys ?? []);
    model.setSelectedKey(key ?? '');
  }

  navigateToNextPage() async {
    if (model.selectedKey.isEmpty) {
      Get.offAllNamed("/welcome");
    } else {
      Get.offAllNamed("/home");
    }
  }
}
