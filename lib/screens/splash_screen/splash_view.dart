import 'package:ether_wallet/globals/app_config.dart';
import 'package:ether_wallet/globals/main_model.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'splash_controller.dart';

class SplashView extends StatelessWidget {
  final SplashController msc;

  SplashView(MainModel model, {Key? key})
      : msc = Get.put(SplashController(model)),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Center(child: SpinKitCubeGrid(color: mainColor, size: 40)),
          Container(
            alignment: Alignment.bottomCenter,
            margin: const EdgeInsets.only(bottom: 40),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                const Text("Ether Wallet", style: TextStyle(fontSize: 15)),
                const SizedBox(height: 5),
                Obx(
                  () => Text("Version:  " + msc.appVersion.value,
                      style: const TextStyle(fontSize: 10)),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
