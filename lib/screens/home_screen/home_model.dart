import 'package:flutter/material.dart';

mixin HomeModel on ChangeNotifier {
  List<String> _accountNames = [];

  List<String> get accountNames => _accountNames;

  void setAccountNames(List<String> accountNames) {
    _accountNames = accountNames;
    notifyListeners();
  }

  List<String> _accountKeys = [];

  List<String> get accountKeys => _accountKeys;

  void setAccountKeys(List<String> accountKeys) {
    _accountKeys = accountKeys;
    notifyListeners();
  }

  String _selectedKey = '';

  String get selectedKey => _selectedKey;

  void setSelectedKey(String selectedKey) {
    _selectedKey = selectedKey;
    notifyListeners();
  }
}
