import 'package:ether_wallet/globals/main_controller.dart';
import 'package:ether_wallet/globals/main_model.dart';
import 'package:ether_wallet/utility/api_manager.dart';
import 'package:ether_wallet/utility/get_utils.dart';
import 'package:ether_wallet/utility/strings_manager.dart';
import 'package:ether_wallet/widgets/edit_name.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:web3dart/web3dart.dart';
import 'package:web_socket_channel/io.dart';

class HomeController extends MainController {
  HomeController(MainModel model) : super(model);
  GlobalKey<ScaffoldState> drawerKey = GlobalKey();
  RxInt contentLoading = 0.obs;
  RxBool nameLoading = false.obs;
  String username = '';
  EthereumAddress? address;
  String balance = '';
  String transactionCount = '';

  saveName(String oldValue, String newValue) async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    List<String> names = model.accountNames;
    names[names.indexWhere((n) => n == oldValue)] = newValue;
    model.setAccountNames(names);
    sp.setStringList(Strings.accountNames, names);
  }

  editName(String nameTemp) {
    Get.dialog(EditNameDialog(name: nameTemp)).then((value) {
      if (value != null) {
        nameLoading(true);
        if (model.accountNames.contains(value)) {
          editName(nameTemp);
          showGetToast('Warning', "This Username has been selected!");
        } else {
          username = value;
          saveName(nameTemp, value);
        }
        nameLoading(false);
      }
    });
  }

  copyToClipboard() {
    Clipboard.setData(ClipboardData(text: address!.hex));
    Get.snackbar('\nCopied to Clipboard', "",
        colorText: Colors.black, backgroundColor: Colors.white70);
  }

  Future<EtherAmount> balanceError() async {
    showGetToast("Error", "Host is down!");
    return EtherAmount.inWei(BigInt.parse('-1'));
  }

  Future<int> countError() async {
    showGetToast("Error", "Host is down!");
    return -1;
  }

  refreshTransactionData() async {
    contentLoading.value++;
    EtherAmount? weiBalance = await model.client
        ?.getBalance(address!)
        .timeout(const Duration(seconds: 10))
        .catchError((e) => balanceError());
    int? count = await model.client
        ?.getTransactionCount(address!)
        .catchError((e) => countError());
    balance = (weiBalance!.getInWei / BigInt.from(10).pow(18)).toString() +
        ' (Ether)';
    transactionCount = count.toString();
    if (count! < 0) {
      balance = 'Host is Down! Please Refresh later';
      transactionCount = 'Host is Down! Please Refresh later';
    }
    contentLoading.value--;
  }

  Future<void> initializeConnection(String privateKey) async {
    contentLoading.value++;
    model.setClient(Web3Client(Apis.rpcUrl, Client(),
        socketConnector: () =>
            IOWebSocketChannel.connect(Apis.wsUrl).cast<String>()));
    model.setCredentials(
        await model.client?.credentialsFromPrivateKey(privateKey));
    if (model.credentials == null) {
      Get.offAllNamed('/login');
    } else {
      address = await model.credentials?.extractAddress();
      refreshTransactionData();
    }
    contentLoading.value--;
  }

  initializeAccount() async {
    contentLoading.value++;
    username = model.accountNames[
        model.accountKeys.indexWhere((k) => k == model.selectedKey)];
    if (model.credentials == null) {
      initializeConnection(model.selectedKey);
    } else {
      address = await model.credentials?.extractAddress();
      refreshTransactionData();
    }
    contentLoading.value--;
  }

  @override
  void onInit() {
    WidgetsBinding.instance?.addPostFrameCallback((_) => initializeAccount());
    super.onInit();
  }
}
