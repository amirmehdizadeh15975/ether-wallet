import 'package:ether_wallet/globals/main_model.dart';
import 'package:ether_wallet/screens/home_screen/home_controller.dart';
import 'package:ether_wallet/utility/style_manager.dart';
import 'package:ether_wallet/widgets/left_drawer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';

class HomeView extends StatelessWidget {
  final HomeController homeC;

  HomeView(MainModel model, {Key? key})
      : homeC = Get.put(HomeController(model)),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    double size = 8.0;
    return SafeArea(
      child: Scaffold(
        key: homeC.drawerKey,
        drawer: const LeftDrawer(),
        appBar: AppBar(
          title: Row(children: [
            Obx(() => Text(
                homeC.nameLoading.value || homeC.contentLoading.value > 0
                    ? "Loading..."
                    : "${homeC.username}'s Profile")),
            const Spacer(),
            IconButton(
                visualDensity:
                    const VisualDensity(horizontal: -4, vertical: -4),
                onPressed: homeC.nameLoading.value
                    ? null
                    : () => homeC.refreshTransactionData(),
                icon: const Icon(Icons.refresh),
                padding: EdgeInsets.zero),
          ]),
        ),
        resizeToAvoidBottomInset: false,
        body: Stack(
          alignment: Alignment.topLeft,
          children: [
            Image.asset('assets/images/greyBackground3.jpg',
                fit: BoxFit.fill,
                height: double.infinity,
                width: double.infinity),
            Obx(
              () => homeC.contentLoading.value > 0
                  ? SpinKitCircle(color: Styles.mainLogo2)
                  : Padding(
                      padding: const EdgeInsets.all(25.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(children: [
                            Padding(
                              padding:
                                  const EdgeInsets.only(right: 5, bottom: 4),
                              child: Icon(Icons.account_circle,
                                  color: Styles.mainLogo2),
                            ),
                            Text("Account Username :",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20,
                                    color: Styles.mainLogo2)),
                            const Spacer(),
                            IconButton(
                                visualDensity: const VisualDensity(
                                    horizontal: -4, vertical: -4),
                                onPressed: homeC.nameLoading.value
                                    ? null
                                    : () => homeC.editName(homeC.username),
                                icon: const Icon(Icons.edit),
                                padding: EdgeInsets.zero),
                          ]),
                          SizedBox(height: size),
                          Obx(
                            () => homeC.nameLoading.value
                                ? SpinKitThreeBounce(
                                    color: Styles.mainLogo2, size: 20)
                                : Text(homeC.username,
                                    style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18)),
                          ),
                          Divider(color: Styles.mainDivider, height: size * 4),
                          Row(children: [
                            Padding(
                              padding:
                                  const EdgeInsets.only(right: 5, bottom: 5),
                              child: Icon(Icons.home_filled,
                                  color: Styles.mainLogo2),
                            ),
                            Text("Address :",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20,
                                    color: Styles.mainLogo2)),
                            const Spacer(),
                            IconButton(
                                visualDensity: const VisualDensity(
                                    horizontal: -4, vertical: -4),
                                onPressed: () => homeC.copyToClipboard(),
                                icon: const Icon(Icons.copy),
                                padding: EdgeInsets.zero),
                          ]),
                          SizedBox(height: size),
                          Text(homeC.address?.hex ?? '',
                              style:
                                  const TextStyle(fontWeight: FontWeight.bold)),
                          Divider(color: Styles.mainDivider, height: size * 4),
                          Row(children: [
                            Padding(
                              padding:
                                  const EdgeInsets.only(right: 5, bottom: 5),
                              child: Icon(Icons.account_balance_wallet,
                                  color: Styles.mainLogo2),
                            ),
                            Text("Balance :",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20,
                                    color: Styles.mainLogo2)),
                          ]),
                          SizedBox(height: size),
                          Text(homeC.balance,
                              style:
                                  const TextStyle(fontWeight: FontWeight.bold)),
                          Divider(color: Styles.mainDivider, height: size * 4),
                          Row(children: [
                            Padding(
                              padding:
                                  const EdgeInsets.only(right: 5, bottom: 5),
                              child: Icon(Icons.credit_card,
                                  color: Styles.mainLogo2),
                            ),
                            Text("Transaction Count :",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20,
                                    color: Styles.mainLogo2)),
                          ]),
                          SizedBox(height: size),
                          Text(homeC.transactionCount,
                              style:
                                  const TextStyle(fontWeight: FontWeight.bold)),
                          Divider(color: Styles.mainDivider, height: size * 4),
                          const Spacer(),
                          Center(
                            child: TextButton(
                                onPressed: () => Get.toNamed('/transaction')!
                                    .then((value) =>
                                        homeC.refreshTransactionData()),
                                child: const Text('New Transaction',
                                    style: TextStyle(fontSize: 18))),
                          ),
                        ],
                      ),
                    ),
            ),
          ],
        ),
      ),
    );
  }
}
