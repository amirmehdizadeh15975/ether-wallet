import 'package:ether_wallet/globals/main_model.dart';
import 'package:ether_wallet/screens/welcome_screen/welcome_controller.dart';
import 'package:ether_wallet/utility/style_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class WelcomeView extends StatelessWidget {
  final WelcomeController welcomeC;

  WelcomeView(MainModel model, {Key? key})
      : welcomeC = Get.put(WelcomeController(model)),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return SafeArea(
      child: Scaffold(
        body: Stack(
          alignment: Alignment.topCenter,
          children: [
            Image.asset('assets/images/greyBackground.jpeg',
                fit: BoxFit.fill,
                height: double.infinity,
                width: double.infinity),
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                    height: height * 0.21 + MediaQuery.of(context).padding.top),
                ClipRect(
                  child: Image.asset('assets/images/etherLogo.png',
                      fit: BoxFit.fitHeight, height: 50, width: 50),
                ),
                const SizedBox(height: 12),
                const Text("Welcome to",
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 16)),
                const Text("Ether Wallet",
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 16)),
                const SizedBox(height: 8),
                const Text('Enjoy experiencing easiest transactions of Ether'),
                Divider(
                    color: Styles.mainDivider,
                    indent: (width - 50) / 2,
                    endIndent: (width - 50) / 2,
                    height: 20),
                const Text('Start with an Account',
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 20)),
                Container(
                  margin: const EdgeInsets.fromLTRB(30, 10, 30, 0),
                  child: const Text(
                      'An Ethereum account is needed to send transactions on Ethereum.',
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 14)),
                ),
                Expanded(child: Container()),
                // Container(
                //   margin: const EdgeInsets.fromLTRB(30, 10, 30, 10),
                //   child: const Text(
                //       'Transaction is the way the external world interacting with the Ethereum network.',
                //       textAlign: TextAlign.center,
                //       style: TextStyle(fontSize: 14)),
                // ),
                TextButton(
                    onPressed: () => Get.offAllNamed('/login'),
                    child: const Text('Get Started',
                        style: TextStyle(fontSize: 18))),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
