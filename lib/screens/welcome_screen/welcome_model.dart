import 'package:flutter/material.dart';

mixin WelcomeModel on ChangeNotifier {
  bool _forceRetry = false;

  bool get forceRetry => _forceRetry;

  setForceRetry(bool forceRetry) {
    _forceRetry = forceRetry;
    notifyListeners();
  }
}
