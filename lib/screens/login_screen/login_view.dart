import 'package:ether_wallet/globals/app_config.dart';
import 'package:ether_wallet/globals/main_model.dart';
import 'package:ether_wallet/utility/style_manager.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'login_controller.dart';

class LoginView extends StatelessWidget {
  final LoginController loginC;

  LoginView(MainModel model, {Key? key})
      : loginC = Get.put(LoginController(model)),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    MainModel model = context.watch<MainModel>();
    const double size = 8;
    return SafeArea(
      child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.all(25),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text("Login",
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 24)),
                const SizedBox(height: size),
                Text(
                    "You must add an account by entering your account's private key!",
                    style: TextStyle(fontSize: 16, color: Styles.mainLogo2)),
                const SizedBox(height: size * 5),
                const Padding(
                    padding: EdgeInsets.all(size),
                    child: Text("Username:",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 16))),
                TextFormField(
                  controller: loginC.nameC,
                  cursorColor: Styles.mainLogo2,
                  maxLines: 1,
                  enabled: !model.loading,
                  decoration: InputDecoration(
                      contentPadding: const EdgeInsets.all(10),
                      filled: true,
                      fillColor: Styles.mainLogo0,
                      border: const OutlineInputBorder(
                          borderSide:
                              BorderSide(width: 0, style: BorderStyle.none),
                          borderRadius: BorderRadius.all(Radius.circular(5))),
                      hintText: "Enter Your Username"),
                ),
                const SizedBox(height: size),
                const Padding(
                    padding: EdgeInsets.all(size),
                    child: Text("Private Key:",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 16))),
                TextFormField(
                  controller: loginC.keyC,
                  cursorColor: Styles.mainLogo2,
                  maxLines: 1,
                  enabled: !model.loading,
                  decoration: InputDecoration(
                      contentPadding: const EdgeInsets.all(10),
                      filled: true,
                      fillColor: Styles.mainLogo0,
                      border: const OutlineInputBorder(
                          borderSide:
                              BorderSide(width: 0, style: BorderStyle.none),
                          borderRadius: BorderRadius.all(Radius.circular(5))),
                      hintText: "Please Enter Your Private Key"),
                ),
                const SizedBox(height: size),
                Center(
                  child: ElevatedButton(
                    child: model.loading
                        ? SpinKitThreeBounce(color: Styles.mainLogo2, size: 20)
                        : const Text('Confirm', style: TextStyle(fontSize: 16)),
                    style: ElevatedButton.styleFrom(
                        primary: mainColor, fixedSize: const Size(90, 40)),
                    onPressed: model.loading ? null : () => loginC.login(),
                  ),
                ),
                Center(
                    child: Image.asset("assets/images/etherLogo.gif",
                        width: Get.width, height: Get.width * 0.75)),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
