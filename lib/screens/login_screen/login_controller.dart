import 'package:ether_wallet/globals/main_controller.dart';
import 'package:ether_wallet/globals/main_model.dart';
import 'package:ether_wallet/utility/api_manager.dart';
import 'package:ether_wallet/utility/get_utils.dart';
import 'package:ether_wallet/utility/strings_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:web3dart/web3dart.dart';
import 'package:web_socket_channel/io.dart';

class LoginController extends MainController {
  LoginController(MainModel model) : super(model);
  final String _privateKey =
      "5693d9bb30807384c2e5a55cb3dfe113570b114b9ea65d219498f070496e64b3";
  final String _privateKey2 =
      "c45cfe0a8dad3ca0fec58235efca2acc32e5a34ed35b9f4e64795f10f21bd31f";
  String deployedName = '';
  TextEditingController nameC = TextEditingController();
  TextEditingController keyC = TextEditingController();

  initialSetup() async {
    model.setClient(Web3Client(Apis.rpcUrl, Client(),
        socketConnector: () =>
            IOWebSocketChannel.connect(Apis.wsUrl).cast<String>()));
  }

  Future<void> getCredentials(String privateKey) async {
    Credentials? _client =
        await model.client?.credentialsFromPrivateKey(privateKey);
    model.setCredentials(_client);
  }

  login() async {
    model.setLoading(true);
    keyC.text = _privateKey;
    model.setCredentials(null);
    if (nameC.text.length < 4) {
      showGetToast('Warning', "Your username's length must be at least 4!");
    } else if (keyC.text.length < 8) {
      showGetToast('Warning', "Your key is not valid!");
    } else if (model.accountNames.contains(nameC.text)) {
      showGetToast('Warning', "This Username has been selected!");
    } else if (model.accountKeys.contains(keyC.text)) {
      showGetToast('Warning', "This key is already registered!");
    } else {
      await getCredentials(keyC.text);
      if (model.credentials == null) {
        showGetToast('Warning', "Your key was not valid!");
      } else {
        await saveAccountInfo();
        Get.offAllNamed('/home');
      }
    }
    model.setLoading(false);
  }

  saveAccountInfo() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    List<String> names = model.accountNames;
    List<String> keys = model.accountKeys;
    names.add(nameC.text);
    keys.add(keyC.text);
    model.setAccountNames(names);
    model.setAccountKeys(keys);
    model.setSelectedKey(keyC.text);
    sp.setStringList(Strings.accountNames, names);
    sp.setStringList(Strings.accountKeys, keys);
    sp.setString(Strings.selectedKey, keyC.text);
  }

  @override
  void onInit() {
    WidgetsBinding.instance?.addPostFrameCallback((_) => initialSetup());
    super.onInit();
  }
}
