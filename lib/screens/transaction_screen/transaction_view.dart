import 'package:ether_wallet/globals/app_config.dart';
import 'package:ether_wallet/globals/main_model.dart';
import 'package:ether_wallet/utility/style_manager.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'transaction_controller.dart';

class TransactionView extends StatelessWidget {
  final TransactionController mtc;

  TransactionView(MainModel model, {Key? key})
      : mtc = Get.put(TransactionController(model)),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    MainModel model = context.watch<MainModel>();
    const double size = 8.0;
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Row(children: [
            const Text("Transfer"),
            const Spacer(),
            IconButton(
                visualDensity:
                    const VisualDensity(horizontal: -4, vertical: -4),
                onPressed: () {},
                icon: const Icon(Icons.refresh),
                padding: EdgeInsets.zero),
          ]),
        ),
        resizeToAvoidBottomInset: false,
        body: Stack(
          alignment: Alignment.topLeft,
          children: [
            Image.asset('assets/images/greyBackground2.jpg',
                fit: BoxFit.fill,
                height: double.infinity,
                width: double.infinity),
            Padding(
              padding: const EdgeInsets.all(25.0),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(height: size * 5),
                    const Padding(
                        padding: EdgeInsets.all(size),
                        child: Text("Send To:",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 16))),
                    TextFormField(
                      controller: mtc.destC,
                      cursorColor: Styles.mainLogo2,
                      maxLines: 1,
                      enabled: !model.loading,
                      decoration: InputDecoration(
                          contentPadding: const EdgeInsets.all(10),
                          filled: true,
                          fillColor: Styles.mainLogo0,
                          border: const OutlineInputBorder(
                              borderSide:
                                  BorderSide(width: 0, style: BorderStyle.none),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5))),
                          hintText: "Enter destination address"),
                    ),
                    const SizedBox(height: size),
                    const Padding(
                        padding: EdgeInsets.all(size),
                        child: Text("Amount:",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 16))),
                    TextFormField(
                      controller: mtc.amountC,
                      cursorColor: Styles.mainLogo2,
                      maxLines: 1,
                      enabled: !model.loading,
                      decoration: InputDecoration(
                          contentPadding: const EdgeInsets.all(10),
                          filled: true,
                          fillColor: Styles.mainLogo0,
                          border: const OutlineInputBorder(
                              borderSide:
                                  BorderSide(width: 0, style: BorderStyle.none),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5))),
                          hintText: "Enter sending ether amount"),
                    ),
                    const SizedBox(height: size * 4),
                    Center(
                      child: ElevatedButton(
                        child: model.loading
                            ? SpinKitThreeBounce(
                                color: Styles.mainLogo2, size: 20)
                            : const Text('Confirm',
                                style: TextStyle(fontSize: 16)),
                        style: ElevatedButton.styleFrom(
                            primary: mainColor, fixedSize: const Size(90, 40)),
                        onPressed: model.loading ? null : () => mtc.transfer(),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
