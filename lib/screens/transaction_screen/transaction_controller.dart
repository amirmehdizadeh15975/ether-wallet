import 'dart:convert';
import 'package:ether_wallet/globals/main_controller.dart';
import 'package:ether_wallet/globals/main_model.dart';
import 'package:ether_wallet/utility/get_utils.dart';
import 'package:ether_wallet/widgets/transaction_result.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:web3dart/web3dart.dart';

class TransactionController extends MainController {
  TransactionController(MainModel model) : super(model);
  late String _abiCode;
  late DeployedContract _contract;
  late EthereumAddress _contractAddress;
  late ContractFunction _transaction;
  late ContractEvent _completed;
  TextEditingController amountC = TextEditingController();
  TextEditingController destC = TextEditingController();

  transfer() async {
    model.setLoading(true);
    EthereumAddress _destinationAddress = EthereumAddress.fromHex(destC.text);
    BigInt? _value = BigInt.tryParse(amountC.text);
    if (_value == null) {
      showGetToast('Warning', "The amount is not valid!");
    } else {
      _value = _value * BigInt.from(10).pow(18);
      EtherAmount etherAmount = EtherAmount.inWei(_value);
      await model.client!
          .sendTransaction(
              model.credentials!,
              Transaction.callContract(
                  contract: _contract,
                  function: _transaction,
                  value: etherAmount,
                  parameters: [_destinationAddress]))
          .then((value) async {
        TransactionReceipt? receipt =
            await model.client?.getTransactionReceipt(value);
        if (receipt?.status ?? false) {
          FilterEvent? event = receipt?.logs.first;
          List<dynamic> results =
              _completed.decodeResults(event?.topics ?? [], event?.data ?? '');
          String from = results[0].hex;
          String to = results[1].hex;
          String amount = (results[2] / BigInt.from(10).pow(18)).toString();
          Get.back();
          Get.bottomSheet(TransactionResult(results: [from, to, amount]));
        } else {
          showGetToast('Something was wrong!',
              receipt?.logs.toString() ?? 'please try again later');
        }
      });
    }
    model.setLoading(false);
  }

  transfer2() async {
    TransactionReceipt? receipt = await model.client?.getTransactionReceipt(
        "0x8fdc09295fd7199b0542f4a58fe72462deae8270b27c95e15764bc7d6d5569b5");
    if (receipt?.status ?? false) {
      FilterEvent? event = receipt?.logs.first;
      List<dynamic> results =
          _completed.decodeResults(event?.topics ?? [], event?.data ?? '');
      String from = results[0].hex;
      String to = results[1].hex;
      String amount = (results[2] / BigInt.from(10).pow(18)).toString();
      Get.back();
      Get.bottomSheet(TransactionResult(results: [from, to, amount]));
    } else {
      showGetToast('Something was wrong!',
          receipt?.logs.toString() ?? 'please try again later');
    }
  }

  Future<void> getDeployedContract() async {
    // Telling Web3dart where our contract is declared.
    _contract = DeployedContract(
        ContractAbi.fromJson(_abiCode, "TransactionHandler"), _contractAddress);

    // Extracting the functions, declared in contract.
    _transaction = _contract.function("transaction");
    _completed = _contract.event('Completed');
  }

  Future<void> getAbi() async {
    // Reading the contract abi
    String abiStringFile =
        await rootBundle.loadString("src/artifacts/TransactionHandler.json");
    var jsonAbi = jsonDecode(abiStringFile);
    _abiCode = jsonEncode(jsonAbi["abi"]);

    _contractAddress =
        EthereumAddress.fromHex(jsonAbi["networks"]["5777"]["address"]);
  }

  initialize() async {
    await getAbi();
    await getDeployedContract();
  }

  @override
  void onInit() {
    WidgetsBinding.instance?.addPostFrameCallback((_) => initialize());
    super.onInit();
  }
}
