import 'dart:async';
import 'package:ether_wallet/globals/main_model.dart';
import 'package:ether_wallet/my_app.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runZonedGuarded(() {
    runApp(MultiProvider(
      providers: [
        ChangeNotifierProvider<MainModel>(create: (_) => MainModel()),
      ],
      child: const MyApp(),
    ));
  }, (error, stackTrace) {
    // print('runZonedGuarded: Caught error in my root zone.\n $error');
//    FirebaseCrashlytics.instance.recordError(error, stackTrace);
  });
//  runApp(MyApp());
}
