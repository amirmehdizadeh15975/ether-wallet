const TransactionHandler = artifacts.require("TransactionHandler") ;

contract("TransactionHandler" , () => {
	it("TransactionHandler Testing" , async () => {
	const TransactionHandler = await TransactionHandler.deployed() ;
	await TransactionHandler.setName("mohammad") ;
	const result = await TransactionHandler.yourName() ;
	assert(result === "mohammad") ;
	});
});
